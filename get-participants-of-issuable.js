const participantsList = document.querySelector(".participants-list");
const participants = new Set();
participantsList.children.forEach(participant => {
  const participantProfileLink = participant.querySelector(".author-link").href.split("/");
  const username = `@${participantProfileLink[participantProfileLink.length - 1]}`;

  participants.add(username);
});

console.log(participants.join(" "));
